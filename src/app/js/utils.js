/*******************************************************************************
 * Globals
 *******************************************************************************/

var debug = new Boolean(false);
/*******************************************************************************
 * Debug
 *******************************************************************************/
function log(msg) {
    if(debug == true) {
        try {
            console.log(msg)
        }
        catch(err) {
            alert(msg)
        }
    }
}

/*******************************************************************************
 * IE detection
 *******************************************************************************/

function isSupported() {	
    if ($.browser.msie) {
        return false;
    }
    else {
        return true;
    }
}


/*******************************************************************************
 * BaseURL
 *******************************************************************************/


function baseURL() {
    var baseURL = window.location.protocol + '//' + window.location.host;
    var args = window.location.pathname.split('/');
        
    if (args[args.length - 1] === '') {
        return baseURL + args.join('/');
    } else if (_.indexOf(args[args.length - 1], '.') !== -1) {
        args.pop();
        return baseURL + args.join('/') + '/';
    } else {
        return baseURL + args.join('/') + '/';
    }
}
  
/*******************************************************************************
 * Color conversion (RGB to HEX)
 *******************************************************************************/    
function componentToHex(c){
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}


/*******************************************************************************
 * String checks
 *******************************************************************************/ 

/*
 * For checking if a string is empty, null or undefined
 * 
 */
function isEmpty(str) {
    return (!str || 0 === str.length);
}
/*
 * For checking if a string is blank, null or undefined
 * 
 */
function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}
/*regex indexof*/
String.prototype.regexIndexOf = function(regex, startpos) {
    var indexOf = this.substring(startpos || 0).search(regex);
    return (indexOf >= 0) ? (indexOf + (startpos || 0)) : indexOf;
}
/*
 * Random number from interval
 * 
 * 
 */
function randomFromInterval(from,to)
{
    return Math.floor(Math.random()*(to-from+1)+from);
}
/*
 * Compare arrays
 * 
 * 
 */
function arraysAreEqual(ary1,ary2){
    return (ary1.join('') == ary2.join(''));
}
/*
 * Element in array
 * 
 * 
 */
function include(arr,obj) {
    return (arr.indexOf(obj) != -1);
}
/*
 * 
 * Radian numbers
 * 
 */
if (typeof(Number.prototype.toRad) === "undefined") {
    Number.prototype.toRad = function() {
        return this * Math.PI / 180;
    }
}

if (typeof(Number.prototype.toDeg) === "undefined") {
    Number.prototype.toDeg = function() {
        return this * 180 / Math.PI;
    }
}

var getNumericPart = function(id) {
    var $num = id.replace(/[^\d]+/, '');
    return $num;
}
