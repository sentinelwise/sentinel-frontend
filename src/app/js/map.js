var map;
function main() {
    var map = new L.Map('map', {
        zoomControl: false,
        center: [58.36662739087651, 26.689578294754025],
        zoom: 16
    });
    esriAttr = 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community';
esriUrl = 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';
    L.tileLayer(esriUrl, {id: 'esri_satellite', attribution: esriAttr}).addTo(map);
    cartodb.createLayer(map, 'https://tonissoo.cartodb.com/api/v2/viz/7e730b18-247a-11e6-99af-0e674067d321/viz.json')
            .addTo(map)
            .on('done', function (layer) {
                layer.setInteraction(true);
                log("layer")
                log(layer)
                document.body.onclick = function (e) {
                    var $drawer = $('#drawer')[0],
                            $target = $(e.target);

                    var clickedOnDrawer = $target.context === $drawer;
                    var clickedInsideDrawer = $target.parents('#drawer').length > 0;

                    if (!clickedOnDrawer && !clickedInsideDrawer) {
                        ui.closeDrawer();
                    }
                };

                layer.on('featureClick', function (e, latlng, pos, data) {
                  console.log(latlng, pos, data);
                    if (!ui.drawerIsOpen() && data.content) {
                        ui.openDrawer(data);
                    }
                });
                layer.on('featureOver', function (e, latlng, pos, data) {
                    $(e.target).css('cursor', 'pointer');
                });
                layer.on('featureOut', function (e, latlng, pos, data, layerNumber) {
                    $('.leaflet-tile-loaded').css('cursor', 'auto');
                });
                layer.on('error', function (err) {
                    //cartodb.log.log('error: ' + err);
                });

                var selectActive = false;
                var areaSelect = null;
                $('#multiselect-points').click(function () {
                    if (selectActive) {
                        areaSelect.remove();
                    } else {
                        areaSelect = L.areaSelect({width: 200, height: 300});
                        areaSelect.on("change", function () {
                            var bnds = this.getBounds();
                            //SELECT * FROM {table_name} WHERE the_geom && ST_SetSRID(ST_MakeBox2D(ST_Point(-73.9980, 40.726), ST_Point(-73.995, 40.723)), 4326)
                            var sql = new cartodb.SQL({user: 'tonissoo'});
                            //sql.execute("SELECT * FROM dorpat_random_pts_v5 WHERE the_geom && ST_SetSRID(ST_MakeBox2D(ST_Point("+bnds._southWest.lat+", "+bnds._southWest.lng+"),ST_Point("+bnds._northEast.lat+", "+bnds._northEast.lng+")), 4326)")
                            var sqlStmt = "SELECT cartodb_id,obj_id,St_X(the_geom),St_Y(the_geom) FROM dorpat_random_pts_v5 WHERE the_geom && ST_SetSRID(ST_MakeBox2D(ST_Point(" + bnds._southWest.lng + ", " + bnds._southWest.lat + "),ST_Point(" + bnds._northEast.lng + ", " + bnds._northEast.lat + ")), 4326)";
                            sqlStmt += ' UNION ALL';
                            sqlStmt += " SELECT cartodb_id,obj_id,St_X(the_geom),St_Y(the_geom) FROM idaviru_pts_all_regex2 WHERE the_geom && ST_SetSRID(ST_MakeBox2D(ST_Point(" + bnds._southWest.lng + ", " + bnds._southWest.lat + "),ST_Point(" + bnds._northEast.lng + ", " + bnds._northEast.lat + ")), 4326)";
                            sql.execute(sqlStmt)
                                    .done(function (data) {
                                        ui.openDrawer(data, 'multi');
                                    })
                                    .error(function (errors) {
                                        // errors contains a list of errors
                                        console.log("errors:" + errors);
                                    })
                        });
                        areaSelect.addTo(map);
                    }
                    selectActive = !selectActive;
                });


            }).on('error', function () {
        //cartodb.log.log("some error occurred");
    });
}
window.onload = main;
