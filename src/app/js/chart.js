var chart = c3.generate({
  bindto: '#deltaGraph',
  size: {
    height: 100
  },
  data: {
    json: [],
    keys: {
      x: 'date',
      value: ['line', 'delta']
    },
    names: {
      delta: 'Displacement (mm)'
    },
    types: {
      delta: 'scatter',
      line: 'line'
    },
    colors: {
      delta: '#f3bd1c',
      line: '#3c3f47'
    }
  },
  point: {
    show: false
  },
  axis: {
    x: {
      type: 'timeseries',
      tick: {
        format: '%m.%Y',
        culling: {
          max: 3
        }
      }
    },
    y: {
      show: false
    }
  },
  legend: {
    show: false
  },
  tooltip: {
    grouped: false
  },
  grid: {
    focus: {
      show: true
    }
  },
  line: {
    connectNull: true
  }
});
