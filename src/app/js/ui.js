var Ui = function() {
  var _this = this;

  _this.refs = {
    drawer: document.getElementById('drawer'),
    drawerClose: document.getElementById('close-button'),
    dangerScore: document.getElementById('dangerScore'),
    drawerBuy: document.getElementById('drawer-buy'),
    drawerPayment: document.getElementById('drawer-payment')
  };

  this.init = function() {
    _this.refs.drawerClose.onclick = function() {
      _this.closeDrawer();
    };
    _this.refs.drawerBuy.onclick = function() {
        $(_this.refs.drawerBuy).fadeOut(function() {
            $(_this.refs.drawerPayment).fadeIn();
        });
    };
  };
  this.init();

  this.openDrawer = function(data, type) {
    $(_this.refs.drawerBuy).show();
    $(_this.refs.drawerPayment).hide();
    if(type == 'multi') {console.log(data);
        $('#drawer-container-multi').show();
        $('#drawer-container-one').hide();
        var html = '';
        for(var i in data.rows) {
            var item = data.rows[i];
            html += '#' + item.cartodb_id + ' (Lat: ' + item.st_x + ' Lng: ' + item.st_y + ')<br>';
        }
        $('#drawer-container-multi-points').html(html);
        $('#drawer-container-amount').html((1.5 * data.rows.length) + ' &euro;');
    } else {
        $('#drawer-container-multi').hide();
        $('#drawer-container-one').show();
        $('#drawer-container-amount').html('1.5 &euro;');
        if (data.displacement_per_year) _this.refs.dangerScore.innerText = d3.format('.2f')(data.displacement_per_year);
        var newData = JSON.parse(data.displacement_over_time);

        var deltas = newData.map(function(e, i) {
          return [i, parseFloat(e.delta)];
        });
        var linearRegression = regression('linear', deltas).points;
        for (var i = 0; i < newData.length; i++) {
          newData[i].line = linearRegression[i][1];
        }
        chart.load({
          data: newData
        });
    }
    
    document.body.classList.add('drawer--open');
  };
  this.closeDrawer = function() {
    document.body.classList.remove('drawer--open');
  };
  this.drawerIsOpen = function() {
    return document.body.classList.contains('drawer--open');
  };
};
var ui = new Ui();
