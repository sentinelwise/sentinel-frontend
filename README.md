# Frontend for cartodb based frontend

http://sentinel-webapp.s3-website-eu-west-1.amazonaws.com

## Setup

```
npm install
```

## Development

This will serve the src dir at http://localhost:8080
```
npm start
```

## Deployment

### Setting up AWS CLI

* Obtain credentials (AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY) for the AWS account the S3 bucket lives in
* Update your `~/.aws/config` with the credentials, e.g.

```
[profile sentinel]
aws_access_key_id = <your access key>
aws_secret_access_key = <your secret access key>
```

### Running deployment

Be sure to set your AWS access keys in `~/.aws/config` and provide a profile name 
```
npm run deploy -- --profile=sentinel
```
